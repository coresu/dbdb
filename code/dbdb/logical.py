class ValueRef(object):
    # 声明预存方法
    def prepare_to_store(self, storage):
        pass

    # 静态方法 转换unicode数据为utf-8编码格式；
    @staticmethod
    def referent_to_string(referent):
        return referent.encode('utf-8') 

    # 静态方法 转换utf-8数据为unicode编码格式；
    @staticmethod
    def string_to_referent(string):
        return string.decode('utf-8')

    # 初始化实例，文件指针初始化为NULL,指针偏移量为0；
    def __init__(self, referent=None, address=0):
        self._referent = referent
        self._address = address

    # 获取当前偏移量
    @property
    def address(self):
        return self._address

    # 调用存储层方法storage.read()获取指针偏移；
    # 经过string_to_referent()方法转换并返回当前指针位置；
    def get(self, storage):
        if self._referent is None and self._address:
            self._referent = self.string_to_referent(storage.read(self._address))
        return self._referent

    # 逻辑层的store方法预存数据文件存储层实力化对象
    # 预存方法在二叉树节点遍历类中被重写，更新当前referent，并传入下一个节点
    def store(self, storage):
        if self._referent is not None and not self._address:
            self.prepare_to_store(storage)
            self._address = storage.write(self.referent_to_string(self._referent))


class LogicalBase(object):

    # 预留node_ref_class属性，在binary_tree.py中的子类BinaryTree完成挂载；
    # 值指针内部类赋值，主要完成从存储成调用数据；
    node_ref_class = None
    value_ref_class = ValueRef

    # 将存储层的整个类传入挂载到实例属性_storage；-------------------------------------------------------
    def __init__(self, storage):
        self._storage = storage
        self._refresh_tree_ref()

    def commit(self):
        self._tree_ref.store(self._storage)
        self._storage.commit_root_address(self._tree_ref.address)

    # 刷新根节点address
    # -----------------------------------这里刷新了_tree_ref根节点指针，包含初始化步骤，是二叉树逻辑控制的入口-----------
    def _refresh_tree_ref(self):
        self._tree_ref = self.node_ref_class(
            address=self._storage.get_root_address())

    # get方法，加锁，刷新根节点；
    # 调用binary_tree.py中_get方法遍历节点，查询所需数据；
    def get(self, key):
        if not self._storage.locked:
            self._refresh_tree_ref()
        return self._get(self._follow(self._tree_ref), key)


    # _follow重要的作用是获得最终所需节点的位置；
    def set(self, key, value):
        if self._storage.lock():
            self._refresh_tree_ref()
        self._tree_ref = self._insert(
            self._follow(self._tree_ref), key, self.value_ref_class(value))

    def pop(self, key):
        if self._storage.lock():
            self._refresh_tree_ref()
        self._tree_ref = self._delete(
            self._follow(self._tree_ref), key)

    # 传参ref调用get方法，不断遍历下一个节点；
    def _follow(self, ref):
        return ref.get(self._storage)

    def __len__(self):
        if not self._storage.locked:
            self._refresh_tree_ref()
        root = self._follow(self._tree_ref)
        if root:
            return root.length
        else:
            return 0
