# This started as a very thin wrapper around a file object, with intent to
# provide an object address on write() and a superblock. But as I was writing
# it, I realised that the user really wouldn't want to deal with the lengths of
# the writen chunks (and Pickle won't do it for you), so this module would have
# to abstract the file object into it's own degenerate key/value store.
# (Degenerate because you can't pick the keys, and it never releases storage,
# even when it becomes unreachable!)

import os
import struct

import portalocker


class Storage(object):
    SUPERBLOCK_SIZE = 4096
    INTEGER_FORMAT = "!Q"
    INTEGER_LENGTH = 8

    # 初始化Storage类，将文件传递到类中作为_f属性；
    # 初始化锁状态为False；
    # 调用_ensure_superblock方法初始化数据文件块头
    def __init__(self, f):
        self._f = f
        self.locked = False
        self._ensure_superblock()

    
    def _ensure_superblock(self):
        self.lock()
        self._seek_end()
        end_address = self._f.tell()
        if end_address < self.SUPERBLOCK_SIZE:
            self._f.write(b'\x00' * (self.SUPERBLOCK_SIZE - end_address))
        self.unlock()

    # 如果当前文件未被锁状态调用portalocker.lock方法以LOCK_EX独占模式锁住文件
    def lock(self):
        if not self.locked:
            portalocker.lock(self._f, portalocker.LOCK_EX)
            self.locked = True
            return True
        else:
            return False

    # 如果当前文件时被锁状态，刷新缓冲区后调用portalocker.unlock方法释放文件锁
    def unlock(self):
        if self.locked:
            self._f.flush()
            portalocker.unlock(self._f)
            self.locked = False

    # 将文件指针从文件头偏移到文件末尾；
    def _seek_end(self):
        self._f.seek(0, os.SEEK_END)

    # 将文件指针偏移到文件头部；
    def _seek_superblock(self):
        self._f.seek(0)

    # struct.unpack以大端模式标准对齐方式转换均已long格式转换二进制串，返回对应字节串；
    def _bytes_to_integer(self, integer_bytes):
        return struct.unpack(self.INTEGER_FORMAT, integer_bytes)[0]

    # struct.unpack以大端模式标准对齐方式，转换为long格式文本，返回对应二进制串；
    def _integer_to_bytes(self, integer):
        return struct.pack(self.INTEGER_FORMAT, integer)

    # 读取8字节长度的文本，并将改字节长度转换为long格式化后长度的值；
    def _read_integer(self):
        return self._bytes_to_integer(self._f.read(self.INTEGER_LENGTH))

    # 数据文件加锁，将字节串长度的值翻译为long格式化后长度的值；
    def _write_integer(self, integer):
        self.lock()
        self._f.write(self._integer_to_bytes(integer))

    # 数据文件加锁，将文件指针偏移到文件末尾；
    # 将当前偏移位置赋值给object_address返回；
    # 赋值long格式长度值；
    # 向文件写入数据；
    def write(self, data):
        self.lock()
        self._seek_end()
        object_address = self._f.tell()
        self._write_integer(len(data))
        self._f.write(data)
        return object_address

    # 将文件指针偏移到address所在位置；
    # 读取8字节长度文本
    def read(self, address):
        self._f.seek(address)
        length = self._read_integer()
        data = self._f.read(length)
        return data

    # 刷新根节点地址；
    def commit_root_address(self, root_address):
        self.lock()
        self._f.flush()
        self._seek_superblock()
        self._write_integer(root_address)
        self._f.flush()
        self.unlock()

    # 文件指针从文件头部开始偏移，读取8字节文本作为根节点地址返回；
    def get_root_address(self):
        self._seek_superblock()
        root_address = self._read_integer()
        return root_address

    # 文件解锁并关闭；
    def close(self):
        self.unlock()
        self._f.close()

    # 将closed()作为属性查看文件状态；
    @property
    def closed(self):
        return self._f.closed
